import React from 'react';
import { connect } from 'react-redux';
import actions from '@frontend/actions';
import Switcher from '@frontend/components/modules/global/switcher';
import ComparisonItem from './modules/item';

const Comparison = ({
	plans,
	planGroups,
	monthCycle,
	setCycle,
}) => {
	return (
		<div className="comparison">
			<Switcher
				onClick={value => setCycle(value)}
				active={monthCycle}
			/>
			<div className="comparison__switcher-info">
				V prípade platby ročne môžete <strong>ušetriť 20+ percent z celkovej sumy</strong> oproti platbe počas 12 mesiacov separátne.
			</div>
			<div className="comparison__table">
				{plans &&
					<div className="grid-4_xs-1_sm-1_md-2-equalHeight-colPadded">
						{
							planGroups.map(planGroup => (
								<div
									key={planGroup}
									className="col"
								>
									<ComparisonItem plan={plans.get(planGroup)} />
								</div>
							))
						}
					</div>
				}
			</div>
		</div>
	)
}

const mapStateToProps = state => ({
	monthCycle: state.company.monthCycle,
});

const mapDispatchToProps = dispatch => ({
	setCycle: payload => dispatch(actions.companySetCycle(payload)),
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Comparison);
