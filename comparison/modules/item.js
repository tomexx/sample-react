import React from 'react';
import { history } from '@frontend/store';
import classNames from 'classnames';
import Button from '@frontend/components/modules/global/button';

const getClassNames = (design) => {
	const classes = {
		comparisonItem__header: true,
	};

	if (design) {
		classes[`u-background-group-${design}`] = true;
	}

	return classNames(classes);
};

const ComparisonItem = function ({
	plan,
}) {
	return (
		<div
			className="comparisonItem"
			onClick={() => {
				history.push(plan.cta.link);
			}}
		>
			<div className={getClassNames(plan.group)}>
				<div className="comparisonItem__icon">
					{plan.icon}
				</div>
				<div className="comparisonItem__name">
					{plan.name}
				</div>
			</div>
			<div className="comparisonItem__pricing">
				<div className="comparisonItem__pricing-wrapper">
					<div className="comparisonItem__price">
						{plan.type === 'free' &&
							<div className="comparisonItem__price-text">{plan.price}</div>
						}
						{plan.type !== 'free' &&
							plan.priceOld !== 0 &&
							<div>
								<div className="comparisonItem__price-label">akčná cena</div>
								<div className="comparisonItem__price">
									<div className="comparisonItem__price-number">{plan.price}</div>
									<div className="comparisonItem__price-currency">€</div>
								</div>
								<div className="comparisonItem__price comparisonItem__price--old">
									<div className="comparisonItem__price-number">
										<span className="comparisonItem__price-number-text">bežne</span>&nbsp;
										<span className="comparisonItem__price-number-value">{plan.priceOld}</span>
									</div>
									<div className="comparisonItem__price-currency">€</div>
								</div>
							</div>
						}
					</div>
				</div>
				{plan.type !== 'free' &&
					plan.priceOld === 0 &&
					<div className="comparisonItem__price">
						<div className="comparisonItem__price-number">{plan.price}</div>
						<div className="comparisonItem__price-currency">€</div>
					</div>
				}
				{plan.type !== 'free' &&
					<div className="comparisonItem__price-period">{plan.billing}</div>
				}
			</div>
			<div className="comparisonItem__description">
				{plan.description}
			</div>
			<div className="comparisonItem__cta">
				<Button
					text={plan.cta.label}
					onClick={() => {
						history.push(plan.cta.link);
					}}
				/>
			</div>
		</div>
	);
};

export default ComparisonItem;
