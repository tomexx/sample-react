import React from 'react';
import classNames from 'classnames';
import { MediaQueryUntilXs, MediaQueryFromXs, MediaQueryFromSm } from '@frontend/components/modules/global/media-query';
import Button from '@frontend/components/modules/global/button';
import formatFeatureAmount from '@shared/format-feature-amount';
import IconLoop from '@frontend/media/icons/01-Interface-Essential/42-Multimedia-Controls/button-loop-1.svg';
import IconCheck from '@frontend/media/icons/01-Interface-Essential/33-Form-Validation/check-1.svg';
import { history } from '@frontend/store';

const formatValue = (value) => {
	let result = formatFeatureAmount(value);
	if (result === 'neobmedzene') {
		result = <IconLoop width={14} alt={result} />;
	}
	if (result === 'áno') {
		result = <IconCheck width={14} alt={result} />;
	}
	return result;
}

const ComparisonTable = function ({
	features,
	plans,
	planGroups,
}) {
	return (
		<div id="comparison-table" className="comparisonTable">
			<div className="grid">
				<div className="col">
					<table
						className="comparisonTable__main"
						role="table"
					>
						<thead>
							<tr className="comparisonTable__row comparisonTable__row--header">
								<th className="comparisonTable__cell comparisonTable__cell--first" role="columnheader" />
								{
									planGroups.map(planGroup => (
										<th
											key={planGroup}
											className="comparisonTable__cell comparisonTable__cell--group"
											role="columnheader"
										>
											<div
												className={`comparisonTable__group u-background-group-${planGroup}`}
												onClick={() => {
													history.push(plans.get(planGroup).cta.link);
												}}
											>
												<MediaQueryUntilXs>
													{plans.get(planGroup).name[0]}
												</MediaQueryUntilXs>
												<MediaQueryFromXs>
													{plans.get(planGroup).name}
												</MediaQueryFromXs>
											</div>
										</th>
									))
								}
							</tr>
						</thead>
						<tbody>
							{

								Array.from(features).map(feature => (
									<tr
										key={feature[0]}
										className={classNames({
											comparisonTable__row: true,
											'comparisonTable__row--dimmed': feature[1].comingSoon,
										})}
									>
										<td className="comparisonTable__cell comparisonTable__cell--first" role="cell">
											{feature[1].comingSoon &&
												<div className="comparisonTable__coming">
													Pripravujeme
												</div>
											}
											<div className="comparisonTable__feature">
												<div className="comparisonTable__feature-headline">
													{feature[1].name}
												</div>
												<div className="comparisonTable__feature-text">
													{feature[1].description}
												</div>
											</div>
										</td>
										{
											planGroups.map((planGroup) => {
												const item = plans.get(planGroup).features.get(feature[0]);
												return (
													<td
														key={planGroup}
														className="comparisonTable__cell comparisonTable__cell--feature"
														role="cell"
													>
														{feature[1].type === Boolean ? formatValue(Boolean(item)) : formatValue(item)}
													</td>
												);
											})
										}
									</tr>
								))
							}
							<MediaQueryFromSm>
								<tr className="comparisonTable__row comparisonTable__row--no-border">
									<td className="comparisonTable__cell comparisonTable__cell--first" role="cell" />
									{
										planGroups.map((planGroup) => {
											const plan = plans.get(planGroup);
											return (
												<td
													key={planGroup}
													className="comparisonTable__cell"
													role="cell"
												>
													<div className="comparisonTable__pricing">
														<div className="comparisonTable__pricing-wrapper">
															<div className="comparisonTable__price">
																{plan.type === 'free' &&
																	<div className="comparisonTable__price-text">{plan.price}</div>
																}
																{plan.type !== 'free' &&
																	plan.priceOld !== 0 &&
																	<div>
																		<div className="comparisonTable__price">
																			<div className="comparisonTable__price-number">{plan.price}</div>
																			<div className="comparisonTable__price-currency">€</div>
																			<div className="comparisonTable__price-period">/ {plan.billingShort}</div>
																		</div>
																		<div className="comparisonTable__price comparisonTable__price--old">
																			<div className="comparisonTable__price-number">
																				<span className="comparisonTable__price-number-text">bežne</span>&nbsp;
																				<span className="comparisonTable__price-number-value">{plan.priceOld}</span>
																			</div>
																			<div className="comparisonTable__price-currency">€</div>
																		</div>
																	</div>
																}
															</div>
														</div>
														{plan.type !== 'free' &&
															plan.priceOld === 0 &&
															<div className="comparisonTable__price">
																<div className="comparisonTable__price-number">{plan.price}</div>
																<div className="comparisonTable__price-currency">€</div>
															</div>
														}
													</div>
												</td>
											);
										})
									}
								</tr>
							</MediaQueryFromSm>
							<MediaQueryFromSm>
								<tr className="comparisonTable__row">
									<td className="comparisonTable__cell comparisonTable__cell--first" role="cell" />
									{
										planGroups.map(planGroup => (
											<td
												key={planGroup}
												className="comparisonTable__cell"
												role="cell"
											>
												<Button
													text={plans.get(planGroup).cta.label}
													onClick={() => {
														history.push(plans.get(planGroup).cta.link);
													}}
												/>
											</td>
										))
									}
								</tr>
							</MediaQueryFromSm>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};

export default ComparisonTable;
